public class Rectangle extends Shape {
    public Rectangle(Point a, Point b){
        points = new Point[2];
        points[0] = new Point(a);
        points[1] = new Point(b);
    }


    @Override
    double getArea() {
        return Math.abs(points[0].getX() - points[1].getX())
                * Math.abs(points[0].getY() - points[1].getY());
    }

    @Override
    double getPerimeter() {
        return (Math.abs(points[0].getX() - points[1].getX())
                + Math.abs(points[0].getY() - points[1].getY())) * 2;
    }

    @Override
    public void printShapeName() {
        System.out.println("Rectangle");
    }
}
