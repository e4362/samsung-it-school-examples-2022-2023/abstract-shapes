public interface ShapeDescription {
    void printShapeName();
    void printPointsAmount();
}
