public abstract class Shape implements ShapeDescription, Movable {
    Point[] points;
    abstract double getArea();
    abstract double getPerimeter();

    @Override
    public void printPointsAmount() {
        System.out.println(points.length);
    }

    @Override
    public void move(double x, double y) {
        for (int i = 0; i < points.length; i++){
            points[i].setX(points[i].getX() + x);
            points[i].setY(points[i].getY() + y);
        }
    }
}
