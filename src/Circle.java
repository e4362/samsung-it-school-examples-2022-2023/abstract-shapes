public class Circle extends Shape {
    double radius;
    public Circle(Point c, double r){
        points = new Point[1];
        points[0] = new Point(c);
        radius = r;
    }
    @Override
    double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void printShapeName() {
        System.out.println("Circle");
    }
}
