public class Triangle extends Shape {
    public Triangle(Point a, Point b, Point c){
        points = new Point[3];
        points[0] = new Point(a);
        points[1] = new Point(b);
        points[2] = new Point(c);

    }

    @Override
    double getArea() {
        double p = getPerimeter() / 2;
        return Math.sqrt(p*(p - points[0].getDistance(points[1]))*
                (p - points[1].getDistance(points[2]))*
                (p - points[2].getDistance(points[0])));
    }

    @Override
    double getPerimeter() {
        return points[0].getDistance(points[1]) +
                points[1].getDistance(points[2]) +
        points[2].getDistance(points[0]);
    }

    @Override
    public void printShapeName() {
        System.out.println("Triangle");
    }
}
